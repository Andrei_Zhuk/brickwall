(function ($) {
    'use strict';

    $.fn.brickwall = function (options) {
        if (this.length === 0 || this.data('init') == 'yep') {
            return;
        }
        this.attr('data-init', 'yep');

        this.each(function (options) {
            return new BrickWall(this, options, this.data());
        });
    }

    $.fn.brickwall.defaultOptions = {
        column: 3, // Количество колонок
        selector: '> div', //
        ignore: null, // Элементы, которые необходимо игнорировать при выборке
        gutter: 0.5, // Внешний отступ элемента в процентах
        showMethod: 'animate', // Метод отображения грида. 'animate', 'static' или 'scroll'
        wrapBox: null, // id обораючего контейнера
        animateSpeed: 0.3, // Скорость анимации в секундах. 0.3 - 3 миллисекунды
        animateDelay: 100, // Время задержки анимации в миллесекундах
        resizeParams: {},
        message: {
            wait: '',
            loading: '',
            complete: ''
        },
        onComplete: function () {
        }
    }

    function BrickWall(container, options, data) {

    }

    BrickWall.prototype = {

    }


    // Automatically find and run brickwall
    $(function () {
        $('.bw-init').brickwall();
    });
})(jQuery);

